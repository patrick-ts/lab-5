using Avalonia.Controls;
using Avalonia.Interactivity;
using EmailManager.Models.Entities;
using EmailManager.Utils;
using EmailManager.Utils.Observable;
using EmailManager.ViewModels;

namespace EmailManager.Views;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();

        NameError.IsVisible = false;
        SurnameError.IsVisible = false;
        EmailError.IsVisible = false;
    }

    private void SendData_Click(object sender, RoutedEventArgs e)
    {
        string? nameTxt = Name.Text;
        string? surnameTxt = Surname.Text;
        string? emailTxt = Email.Text;

        string errorMessage = Validator.ValidateUserFields(nameTxt, surnameTxt, emailTxt);

        if (string.IsNullOrEmpty(errorMessage))
        {
            MainWindowViewModel.AddUser(nameTxt, surnameTxt, emailTxt);

            int count = MainWindowViewModel.CountUsers();
            Count.Text = $"Count: {count}";

            ClearFields();
        }
        else
        {
            if (string.IsNullOrWhiteSpace(nameTxt))
            {
                NameError.Text = errorMessage;
                NameError.IsVisible = true;
                Subject.Add(new SystemNotify());
                Subject.Notify(errorMessage);
            }
            else if (string.IsNullOrWhiteSpace(surnameTxt))
            {
                SurnameError.Text = errorMessage;
                SurnameError.IsVisible = true;
                Subject.Add(new SystemNotify());
                Subject.Notify(errorMessage);
            }
            else if (string.IsNullOrWhiteSpace(emailTxt) || !Validator.EmailValidator(emailTxt))
            {
                EmailError.Text = errorMessage;
                EmailError.IsVisible = true;
                Subject.Add(new SystemNotify());
                Subject.Notify(errorMessage);
            }
        }
    }

    public void SearchUser_Click(object sender, RoutedEventArgs e)
    {
        if (!string.IsNullOrEmpty(Search.Text))
        {
            MainWindowViewModel.SearchUserByEmail(Search.Text);
            int count = MainWindowViewModel.CountUsers();
            Count.Text = $"Count: {count}";
        }
        else
        {
            MainWindowViewModel.List();
            int count = MainWindowViewModel.CountUsers();
            Count.Text = $"Count: {count}";
        }
    }

    public void OrderAsc_Click(object sender, RoutedEventArgs e)
    {
        MainWindowViewModel.OrderAsc();
    }

    public void OrderDsc_Click(object sender, RoutedEventArgs e)
    {
        MainWindowViewModel.OrderDsc();
    }

    private void ClearFields()
    {
        Name.Text = "";
        Surname.Text = "";
        Email.Text = "";

        NameError.Text = "";
        NameError.IsVisible = false;
        SurnameError.Text = "";
        SurnameError.IsVisible = false;
        EmailError.Text = "";
        EmailError.IsVisible = false;
    }
}
