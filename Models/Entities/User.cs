namespace EmailManager.Models.Entities;

public class User
{
    public string Name { get; set; }
    public string Surname { get; set; }
    public string Email { get; set; }

    public User(string name, string surname, string email)
    {
        Name = name;
        Surname = surname;
        Email = email;
    }

    public override string ToString()
    {
        return $"Name: {Name} {Surname}, Email: {Email}";
    }
}
