using System;
using EmailManager.Utils.Observable;

namespace EmailManager.Models.Entities;

public class SystemNotify : INotifyObserver
{
    public void Update(string message)
    {
        Console.WriteLine(message);
    }
}
