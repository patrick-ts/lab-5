using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using EmailManager.Models.Entities;

namespace EmailManager.Models.Repositories;

public class InMemoryUserRepository
{
    private readonly ObservableCollection<User> _users = [];

    public void AddUser(User newUser) => _users.Add(newUser);

    public List<User> FilterUsersByEmail(string email)
    {
        return _users.Where(user => user.Email.Contains(email)).ToList();
    }

    public ObservableCollection<User> List() => _users;

    public ObservableCollection<User> ListAsc() => [.. _users.OrderBy(user => user.Name)];

    public ObservableCollection<User> ListDsc()
    {
        var query = from user in _users orderby user.Email descending select user;

        return [.. query];
    }
}
