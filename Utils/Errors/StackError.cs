using System;
using System.Collections.Generic;

namespace EmailManager.Utils.Errors;

public static class StackError
{
    private static readonly Stack<Exception> StackErrors = new();

    public static void Add(string message)
    {
        StackErrors.Push(new Exception($"{message} {DateTime.Now}"));

        if (StackErrors.Count > 3) Remove();
    }

    public static void Remove()
    {
        if (StackErrors.Count == 0) return;

        for (var i = 0; i < StackErrors.Count; i++)
        {
            var error = StackErrors.Pop();
            Console.WriteLine(error.Message);
        }
    }
}
