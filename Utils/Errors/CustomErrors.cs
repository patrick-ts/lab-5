using System;

namespace EmailManager.Utils.Errors;

public abstract class CustomErrors : Exception
{
    public static string InvalidParamError(string param)
    {
        return $"Invalid param: {param}";
    }

    public static string InvalidEmail(string email)
    {
        return $"Invalid email: {email}";
    }
}
