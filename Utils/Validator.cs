using System.Text.RegularExpressions;
using EmailManager.Utils.Errors;

namespace EmailManager.Utils;

public abstract class Validator
{
    public static string ValidateUserFields(string name, string surname, string email)
    {
        if (string.IsNullOrWhiteSpace(name))
        {
            StackError.Add(CustomErrors.InvalidParamError("Name"));
            return "Nome is required.";
        }

        if (string.IsNullOrWhiteSpace(surname))
        {
            StackError.Add(CustomErrors.InvalidParamError("Surname"));
            return "Surname is required.";
        }

        if (string.IsNullOrWhiteSpace(email))
        {
            StackError.Add(CustomErrors.InvalidParamError("Email"));
            return "Email is required.";
        }

        if (EmailValidator(email)) return "";

        StackError.Add(CustomErrors.InvalidEmail("Email"));
        return "Invalid email format.";
    }

    public static bool EmailValidator(string name)
    {
        Regex regex = new(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$");

        return regex.IsMatch(name);
    }
}
