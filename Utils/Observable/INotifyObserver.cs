namespace EmailManager.Utils.Observable;

public interface INotifyObserver
{
    void Update(string message);
}
