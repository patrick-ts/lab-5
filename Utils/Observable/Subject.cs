using System;
using System.Collections.Generic;

namespace EmailManager.Utils.Observable;

public static class Subject
{
    private static readonly List<INotifyObserver> Observers = [];

    public static void Add(INotifyObserver notifyObserver)
    {
        Observers.Add(notifyObserver);
    }

    public static void Remove(INotifyObserver notifyObserver)
    {
        Observers.Remove(notifyObserver);
    }

    public static void Notify(string message)
    {
        try {
            foreach (var observer in Observers)
            {
                observer.Update(message);

            }
        } catch (Exception err) {
            Console.WriteLine(err.Message);
        }
    }
}
