﻿using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;
using EmailManager.Models.Entities;
using EmailManager.Models.Repositories;

namespace EmailManager.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    private static readonly InMemoryUserRepository UserRepository = new();

    public static ObservableCollection<User> Users { get; set; } = [];

    public static void AddUser(string name, string surname, string email)
    {
        var newUser = new User(name, surname, email);

        UserRepository.AddUser(newUser);

        Users.Add(newUser);
    }

    public static void SearchUserByEmail(string email)
    {
        var filteredUsers = UserRepository.FilterUsersByEmail(email!.Trim());
        Users.Clear();

        foreach (var user in filteredUsers)
        {
            Users.Add(user);
        }
    }

    public static void OrderAsc()
    {
        RemakeUserList(UserRepository.ListAsc());
    }

    public static void OrderDsc()
    {
        RemakeUserList(UserRepository.ListDsc());
    }

    public static int CountUsers()
    {
        return Users.Count;
    }

    public static void List()
    {
        RemakeUserList(UserRepository.List());
    }

    public static void RemakeUserList(ObservableCollection<User> users)
    {
        Users.Clear();

        foreach (User user in users)
        {
            Users.Add(user);
        }
    }
}
